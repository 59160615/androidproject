package com.example.myapplication.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.example.myapplication.R
import com.example.myapplication.Recipe
import com.example.myapplication.RecycleAdapter
import com.example.myapplication.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentHomeBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_home, container, false)
        val recipeList = Recipe.getRecipesFromFile("recipes.json", context!!)
        val adapter: RecycleAdapter = RecycleAdapter(context!!, recipeList!!)
        binding.recyclerList.adapter = adapter


        return binding.root
    }


}
