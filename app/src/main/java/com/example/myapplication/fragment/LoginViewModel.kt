package com.example.myapplication.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoginViewModel: ViewModel() {
    private var _isLogin = MutableLiveData<Boolean>()
    val isLogin: LiveData<Boolean>
        get() = _isLogin

    fun loginSuccess() {
        _isLogin.value = true
    }

    fun onDone() {
        _isLogin.value = null
    }
}