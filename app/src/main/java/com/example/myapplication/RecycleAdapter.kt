package com.example.myapplication

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
//import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main_detail.view.*
import kotlinx.android.synthetic.main.item.view.*
import kotlinx.android.synthetic.main.login.view.*

class RecycleAdapter (val context: Context, private val items : ArrayList<Recipe>) :
    RecyclerView.Adapter<ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item, parent, false),context)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindView(items[position])
    }

}

 class ViewHolder (view: View,context: Context) : RecyclerView.ViewHolder(view) {

    fun bindView(recipe: Recipe) {
        Picasso.get().load(recipe.imageUrl).placeholder(R.mipmap.ic_launcher)
            .into(itemView.recipe_list_thumbnail)

        itemView.recipe_linkUrl.text = recipe.imageUrl
        itemView.recipe_list_title.text = recipe.title
        itemView.recipe_list_subtitle.text = recipe.description
        itemView.recipe_des2.text = recipe.description2
        itemView.recipe_stadium.text = recipe.stadium
        itemView.recipe_win.text = recipe.win
        itemView.recipe_lose.text = recipe.lose


    }

    init {
        itemView.setOnClickListener {
            Recipe?.let {
                Log.i("aaaaa","onClickImg")
            }
        }

        itemView.recipe_list_text_layout.setOnClickListener {

            Recipe?.let {

                Log.i("aaaaa","onClick")
                val intent = Intent(context, MainDetail::class.java)

                intent.putExtra("imgUrl", itemView.recipe_linkUrl.text.toString())
                intent.putExtra("title", itemView.recipe_list_title.text.toString())
                intent.putExtra("subtitle", itemView.recipe_list_subtitle.text.toString())
                intent.putExtra("des2", itemView.recipe_des2.text.toString())
                intent.putExtra("stadium", itemView.recipe_stadium.text.toString())
                intent.putExtra("win", itemView.recipe_win.text.toString())
                intent.putExtra("lose", itemView.recipe_lose.text.toString())


                context.startActivity(intent)

            }
        }
    }
}