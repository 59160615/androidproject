package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class Login : AppCompatActivity() {
    private val button: Button? = null
    private var User: EditText? = null
    private var Pw: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        val button = findViewById<View>(R.id.button) as Button

        User = findViewById(R.id.username) as EditText
        Pw = findViewById(R.id.password) as EditText

        button.setOnClickListener {
            if(User!!.text.toString() == "" && Pw!!.text.toString() == "") {
                Toast.makeText(this, "Click Fail Try Again", Toast.LENGTH_LONG).show()
            }else if (Pw!!.text.toString() == "") {
                Toast.makeText(this, "Click Fail Try Again", Toast.LENGTH_LONG).show()
            }else if (User!!.text.toString() == "") {
                Toast.makeText(this, "Click Fail Try Again", Toast.LENGTH_LONG).show()
            }else if(User!!.text.toString() == "test" && Pw!!.text.toString() == "1234"){
                connect()
            }else{
                Toast.makeText(this, "Click Fail Try Again", Toast.LENGTH_LONG).show()
            }


        }
    }

    private fun connect() {

        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("title", "TEST")
        startActivity(intent)
        Toast.makeText(applicationContext,"Welcome", Toast.LENGTH_SHORT).show()

    }
}