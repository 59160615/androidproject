package com.example.myapplication

import android.content.Context
import org.json.JSONException
import org.json.JSONObject

class Recipe(val title: String, val description: String,val description2: String , val imageUrl: String,val stadium: String,val win:String,val lose:String)  {
    companion object {
        fun getRecipesFromFile(filename: String, context: Context): ArrayList<Recipe> {
            val recipeList = ArrayList<Recipe>()
            try {
                //Load data
                val jsonString = loadJsonFromAsset("recipes.json", context)
                val json = JSONObject(jsonString)
                val recipes = json.getJSONArray("recipes")
                // Get Recipe objects from data
                (0 until recipes.length()).mapTo(recipeList) {

                    Recipe(recipes.getJSONObject(it).getString("title"),
                        recipes.getJSONObject(it).getString("description"),
                        recipes.getJSONObject(it).getString("description2"),
                        recipes.getJSONObject(it).getString("image"),
                        recipes.getJSONObject(it).getString("stadium"),
                        recipes.getJSONObject(it).getString("win"),
                        recipes.getJSONObject(it).getString("lose")
                    )



                }
            }catch (e: JSONException){
                e.printStackTrace()
            }

            return recipeList
        }//end fun getRecipesFromFile

        private fun loadJsonFromAsset(filename: String, context: Context): String? {
            lateinit var json: String
            try {
                val inputStream = context.assets.open(filename)
                val size = inputStream.available()
                val buffer = ByteArray(size)
                inputStream.read(buffer)
                inputStream.close()
                json = String(buffer, Charsets.UTF_8)
            }catch (ex: java.io.IOException){
                ex.printStackTrace()
                return null
            }
            return json
        }
    }
}