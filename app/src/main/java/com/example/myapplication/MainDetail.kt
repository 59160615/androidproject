package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main_detail.*
import kotlinx.android.synthetic.main.item.*
import kotlinx.android.synthetic.main.item.view.*

class MainDetail : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_detail)

        val imgUrl_val= intent.getStringExtra("imgUrl")
        Picasso.get().load(imgUrl_val).placeholder(R.mipmap.ic_launcher)
            .into(recipe_list_image)
        titlehead!!.text = intent.getStringExtra("title")
        subtitle!!.text = intent.getStringExtra("subtitle")
        des2!!.text = intent.getStringExtra("des2")
        stadium!!.text = intent.getStringExtra("stadium")
        win!!.text = intent.getStringExtra("win")
        lose!!.text = intent.getStringExtra("lose")


    }
}